module.exports = (mongoose) => {
    const setupHook = require('../hooks/product.hooks');
    
    // ===> Create Schemas first
    // Main Schema
    schema = mongoose.Schema({
        dijual: {
            type: Boolean,
            required: true,
            default: true
        },
        namaProduct: {
            type: String,
            required: true
        },
        stockProduct: {
            type: Number,
            required: true
        },
        hargaJual: {
            type: Number,
            required: true
        },
        category: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'category',
            required: true
        },
        foto: {
            type: String,
            required : true
        }
    },
    {
        timestamps: true
    });

    // ===> Set-up Hooks
    setupHook(schema);

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });

    return mongoose.model("product", schema);
}