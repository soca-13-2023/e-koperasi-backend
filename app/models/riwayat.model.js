const setupHooks = require('../hooks/riwayat.hooks');
// const { setupOngoingBuy } = require('../controllers/pembayaran.controller');

module.exports = (mongoose) => {
    // ===> Create Schemas first
    const productSchema = mongoose.Schema({}, { discriminatorKey: 'status', _id: false })
    const riwayatSchema = mongoose.Schema(
        {
            product: productSchema,
            tanggal: {
                type: Date,
                required: true
            }
        }, { discriminatorKey: 'type' }
    );

    const jualSchema = mongoose.Schema({
        hargaJual: {
            type: Number,
            required: true
        },
        oleh: {
            type: String,
            ref: 'account',
            required: true
        },
        jumlah: {
            type: Number,
            required: true
        },
        riwayatPembayaran: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'riwayat'
        }
    });
    const beliSchema = mongoose.Schema({
        hargaBeli: {
            type: Number,
            required: true
        },
        jumlah: {
            type: Number,
            required: true
        }
    });

    const tambahSchema = mongoose.Schema({
        jumlah: {
            type: Number,
            required: true
        }
    });
    const kurangSchema = mongoose.Schema({
        jumlah: {
            type: Number,
            required: true
        }
    });

    const pembayaranSchema = mongoose.Schema({
        statusPembayaran: {
            type: String,
            enum: ['lunas', 'ditolak', 'dibatalkan'],
            required: true
        }
    })


    // Main Schema
    schema = mongoose.Schema(
        riwayatSchema, { timestamps: true }
    );

    // ===> Set-up Discriminator
    schema.discriminator('jual', jualSchema);
    schema.discriminator('beli', beliSchema);
    schema.discriminator('kurang', kurangSchema);
    schema.discriminator('tambah', tambahSchema);
    schema.discriminator('pembayaran', pembayaranSchema);

    riwayatSchema.path('product').discriminator('exist', mongoose.Schema({
        data: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'product'
        }
    }, { _id: false }));
    riwayatSchema.path('product').discriminator('deleted', mongoose.Schema({
        namaProduct: String
    }, { _id: false }));

    // ===> Set-up Hooks
    setupHooks(schema)
    // setupOngoingBuy();

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });

    return mongoose.model("riwayat", schema)
}