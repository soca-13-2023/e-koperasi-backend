module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            _id: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            type: {
                type: String,
                enum: ['user', 'admin'],
                required: true
            },
            password: {
                type: String,
                required: true
            },
            foto: {
                type: String,
                required: true
            }
        }, { discriminatorKey: 'type' })
    const userSchema = mongoose.Schema({
            telpon: {
                type: Number,
                required: true
            },
            nis: {
                type: Number,
                required: true
            },
            tanggalLahir: {
                type: Date,
                required: true
            },
            kelas: {
                type: String,
                required: true
            }
        })
    const adminSchema = mongoose.Schema({
            nik: {
                type: Number,
                required: true
            }
        })
    {
        timestamps: true
    }
    
    schema.discriminator('user', userSchema)
    schema.discriminator('admin', adminSchema)

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });
    
    const model = mongoose.model("account", schema);
   
    return model;
};
