module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            namaCategory: {
                type: String,
                required: true
            },
            foto: {
                type: String,
                required: true
            }
        },
        {
            timestamps: true
        }
    );

    schema.method("toJSON", function(){
        const{__v,_id, ...object} = this.toObject();
        object.id = _id;

        return object;
    });
    
    const model = mongoose.model("category", schema);
   
    return model;
};
