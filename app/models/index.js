const dbConfig = require("../config/database");
const mongoose = require("mongoose");


const exportList = {
    mongoose,
    url: dbConfig.url,
    category: require('./category.model')(mongoose),
    // product: new (require('./product.model'))(mongoose, { lazySetupModel: false }),
    product: require('./product.model')(mongoose),
    // transaksi: new (require('./transaksi.model'))(mongoose, { lazySetupModel: false })
    riwayat: require('./riwayat.model')(mongoose),
    account: require('./account.model')(mongoose)
};
global.database = exportList;

module.exports = exportList
