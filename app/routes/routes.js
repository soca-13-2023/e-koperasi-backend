module.exports = app => {
    const { body, checkSchema, validationResult } = require('express-validator');
    const account = require("../controllers/account.controller");
    const product = require("../controllers/product.controller");
    const category = require("../controllers/category.controller");
    const riwayat = require("../controllers/riwayat.controller");
    const pendapatan = require("../controllers/pendapatan.controller");
    const pengeluaran = require("../controllers/pengeluaran.controller");
    const pembayaran = require('../controllers/pembayaran.controller')
    const laporan = require("../controllers/laporan.controller");
    const upload = (require("multer"))({ dest: 'public/foto/' })
    const r = require("express").Router();

    const validateInput = (schema) => {
        const validator = checkSchema(schema);

        return async (req, res, next) => {
            await validator.run(req);
            const error = validationResult(req);
            if (!error.isEmpty()) {
                return res.status(400).send({ status: 'invalid', message: error.errors })
            }
            next();
        }
    }

    r.post("/account/register", account.checkIfLoggedIn, account.register);
    r.post("/account/login", account.login);
    r.post("/account/logout", account.authenticateAll, account.logout);
    r.get("/account", account.authenticateAll, account.getAccountInfo)
    r.get("/account/:id", account.authenticateAll, account.getAccountInfo)
    r.put("/account/:id", account.authenticateAll, account.update)
    r.delete("/account/:id", account.authenticateAll, account.delete)

    r.get("/product", product.findAll);
    r.get("/product/:id", product.showAll);
    r.post("/product", product.create);
    r.put("/product/:id", account.authenticateAdmin, product.update);
    r.delete("/product/:id", account.authenticateAdmin, product.delete);
    r.get("/product/getbycategory/:id", product.findProductByCategoryAll);

    r.get("/category", category.findAll);
    r.get("/category/:id", category.show);
    r.post("/category",
        account.authenticateAdmin,
        upload.single("foto"), 
        category.create);
    r.put("/category/:id", account.authenticateAdmin, category.update);
    r.delete("/category/:id", account.authenticateAdmin, category.delete);

    r.get("/riwayat", account.authenticateAdmin, riwayat.findAll);
    r.get("/riwayat/:id", account.authenticateAdmin, riwayat.show);
    r.post("/riwayat", account.authenticateAdmin, riwayat.create);
    r.put("/riwayat/:id", account.authenticateAdmin, riwayat.update);
    r.delete("/riwayat/:id", account.authenticateAdmin, riwayat.delete);
    r.get("/riwayat/getRiwayatByType/:type", account.authenticateAdmin, riwayat.getRiwayatByType);
    

    r.get("/pendapatan", pendapatan.getTotal);
    r.get("/pendapatan/year/:year", pendapatan.getPerYear);
    r.get("/pendapatan/month/:month", pendapatan.getPerMonth);

    r.get("/pengeluaran", pengeluaran.getTotal);
    r.get("/pengeluaran/year/:year", pengeluaran.getPerYear);
    r.get("/pengeluaran/month/:month", pengeluaran.getPerMonth);

    r.get("/laporan", laporan.getLaporan);

    r.get("/pembayaran/getPembayaranFromUser/:id", account.authenticateAll, pembayaran.getPembayaranFromUser);
    r.get("/pembayaran/getPembayaranUser", account.authenticateAll, pembayaran.getPembayaranThisUser);
    r.get("/pembayaran/getPembayaranById", account.authenticateAll, pembayaran.tolak)
    r.get("/pembayaran/getPembayaranBelumLunas", account.authenticateAdmin, pembayaran.getPembayaranBelumLunas)
    r.get("/pembayaran/getPembayaran", account.authenticateAdmin, pembayaran.getPembayaran)
    r.post("/pembayaran/checkout", account.authenticateAll, pembayaran.checkout)
    r.post("/pembayaran/bayar/:id", account.authenticateAll, pembayaran.bayar)
    r.post("/pembayaran/tolak/:id", account.authenticateAll, pembayaran.tolak)
    r.post("/pembayaran/cancel/:id", account.authenticateAll, pembayaran.tolak)

    app.use("/", r)
}

