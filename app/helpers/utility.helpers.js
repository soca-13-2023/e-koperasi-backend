const { UnauthorizedError } = require('express-jwt');
const { checkSchema, validationResult } = require('express-validator');
const { JsonWebTokenError } = require('jsonwebtoken');
const { CastError } = require('mongoose')

exports.HookError = class HookError extends Error {
    constructor(message) {
        super(message);
        this.name = 'HookError';
    }
}

exports.InvalidInputError = class InvalidInputError extends Error {
    constructor(message) {
        super(message);
        this.name = 'InvalidInputError';
    }
}

exports.ProhibitedError = class ProhibitedError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ProhibitedError';
    }
}

exports.bulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
]

exports.validateInput = async (req, res, schema) => {
    await checkSchema(schema).run(req);
    const error = validationResult(req);
    if (!error.isEmpty()) {
        res.status(400).send({ status: 'invalid', message: error.errors })
        return false;
    }
    return true;
}

exports.handleError = (message, err, res) => {
    console.error(message + ';', err);
    if (err instanceof this.HookError)
        res.status(404).send({ status: 'failed', message: err.message });
    else if (err instanceof CastError)
        res.status(400).send({ status: 'invalid', message: "Terdapat kesalahan dalam penginputan data" });
    else if (err instanceof this.InvalidInputError)
        res.status(400).send({ status: 'invalid', message: err.message });
    else if (err instanceof this.ProhibitedError)
        res.status(403).send({ status: 'failed', message: err.message });
    else if (err instanceof JsonWebTokenError)
        res.status(403).send({ status: 'failed', message: "Akses ditolak, Pastikan Anda telah melakukan login" });
    else
        res.status(500).send({ status: 'failed', message: "Terjadi kesalahan dari server" });
}