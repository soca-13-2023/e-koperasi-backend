const { validateInput, handleError, InvalidInputError, ProhibitedError } = require('../helpers/utility.helpers')
const Riwayat = global.database.riwayat;
const Product = global.database.product;
const crypto = require('crypto');

exports.getPembayaranById = async (req, res) => {
    try {
        if (await validateInput(req, res, {
            'id': {
                notEmpty: true
            }
        }) == false) return;
    
        Riwayat.find({ _id: req.params.id })
        .populate('product.data', 'namaProduct')
        .populate('riwayatPembayaran', ['statusPembayaran', 'tanggal'])
        .then(data => {
            if (!data)
                throw new InvalidInputError("Tidak ada pembayaran dengan Id yang diberikan");

            if (req.user.id != data.oleh._id) {
                throw new ProhibitedError('Anda tidak boleh mengambil data pembayaran milik orang lain');
            }

            res.json(data);
        })
        .catch(err => {
            handleError('Gagal mendapatkan data Pembayaran berdasarkan Id');
        });
    } catch (err) {
        handleError('Gagal mendapatkan data Pembayaran berdasarkan Id', err, res);
    }
}

exports.getPembayaranFromUser = async (req, res) => {
    try {
        if (req.user.id != req.params.id || req.user.type != 'admin') {
            throw new ProhibitedError('Anda tidak boleh mengambil data pembayaran milik orang lain')
        }   
    
        Riwayat.find({ oleh: req.params.id })
        .populate('product.data', 'namaProduct')
        .populate('riwayatPembayaran', ['statusPembayaran', 'tanggal'])
        .then(data => {
            if (!data)
                throw new InvalidInputError("Tidak ada pembayaran dengan Id yang diberikan");

            res.json(data);
        })
        .catch(err => {
            handleError('Gagal mengambil data pembayaran dari seorang User', err, res);
        });
    } catch (err) {
        handleError('Gagal mengambil data pembayaran dari seorang User', err, res);
    } 
}

exports.getPembayaranThisUser = async (req, res) => {
    try {
        Riwayat.find({ oleh: req.user.id })
        .populate('product.data', 'namaProduct')
        .populate('riwayatPembayaran', ['statusPembayaran', 'tanggal'])
        .then(data => {
            if (!data)
                throw new InvalidInputError("Tidak ada pembayaran dengan Id yang diberikan");

            res.json(data);
        })
        .catch(err => {
            handleError('Gagal mengambil data pembayaran dari seorang User', err, res);
        });
    } catch (err) {
        handleError('Gagal mengambil data pembayaran dari seorang User', err, res);
    } 
}

exports.getPembayaranBelumLunas = async (req, res) => {
    try {
        Riwayat.find({ type: 'jual', riwayatPembayaran: null })
        .populate('oleh', 'nama')
        .populate('product.data', ['namaProduct', 'foto'])
        .populate('riwayatPembayaran', ['statusPembayaran', 'tanggal'])
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            handleError('Gagal mendapatkan semua data Pembayaran yang belum lunas', err, res);
        });
    } catch (err) {
        handleError('Gagal mendapatkan semua data Pembayaran yang belum lunas', err, res);
    }
}

exports.getPembayaran = async (req, res) => {
    try {
        Riwayat.find({ type: 'jual' })
        .populate('oleh', 'nama')
        .populate('product.data', 'namaProduct')
        .populate('riwayatPembayaran', ['statusPembayaran', 'tanggal'])
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            handleError('Gagal mendapatkan semua data Pembayaran', err, res);
        });
    } catch (err) {
        handleError('Gagal mendapatkan semua data Pembayaran', err, res);
    }
}

exports.cancel = async (req, res) => {
    const errMessage = 'Gagal mengcancel Pembayaran';
    try {
        const riwayatPembayaran = await Riwayat.create({
            'type': 'pembayaran',
            'tanggal': Date.now(),
            'statusPembayaran': 'dicancel'
        });

        Riwayat.findById(req.params.id)
        .populate('product.data', '_id')
        .then(async data => {
            if (!data)
                throw new InvalidInputError('Tidak ada Pembayaran dengan Id yang diberikan');

            if (data.riwayatPembayaran != null)
                throw new InvalidInputError('Pembayaran sudah sempat dibeli, ditolak, atau dicancel');

            data.riwayatPembayaran = riwayatPembayaran;
            await data.save();

            decrementProductOngoingBuy(data.product._doc.data._id, data.jumlah);

            res.send({
                status: 'success',
                message: 'Pembayaran berhasil dicancel'
            })
        })
        .catch(err => {
            Riwayat.findByIdAndRemove(riwayatPembayaran._id);
            handleError(errMessage, err, res)
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.tolak = async (req, res) => {
    const errMessage = 'Gagal menolak Pembayaran';
    try {
        const riwayatPembayaran = await Riwayat.create({
            'type': 'pembayaran',
            'tanggal': Date.now(),
            'statusPembayaran': 'ditolak'
        });

        Riwayat.findById(req.params.id)
        .populate('product.data', '_id')
        .then(async data => {
            if (!data)
                throw new InvalidInputError('Tidak ada Pembayaran dengan Id yang diberikan');

            if (data.riwayatPembayaran != null)
                throw new InvalidInputError('Pembayaran sudah sempat dibeli, ditolak, atau dicancel');

            data.riwayatPembayaran = riwayatPembayaran;
            await data.save();

            decrementProductOngoingBuy(data.product._doc.data._id, data.jumlah);

            res.send({
                status: 'success',
                message: 'Pembayaran berhasil ditolak'
            })
        })
        .catch(err => {
            Riwayat.findByIdAndRemove(riwayatPembayaran._id);
            handleError(errMessage, err, res)
        });
    } catch (err) {
        handleError(errMessage, err, res)
    }
}

exports.bayar = async (req, res) => {
    const errMessage = 'Gagal melakukan Bayar';
    try {
        const riwayatPembayaran = await Riwayat.create({
            'type': 'pembayaran',
            'tanggal': Date.now(),
            'statusPembayaran': 'lunas'
        });

        Riwayat.findById(req.params.id)
        .populate('product.data', '_id')
        .then(async data => {
            if (!data)
                throw new InvalidInputError('Tidak ada Pembayaran dengan Id yang diberikan');

            if (data.riwayatPembayaran != null)
                throw new InvalidInputError('Pembayaran sudah sempat dibeli, ditolak, atau dicancel');

            // Cek dulu apakah produknya ada dan stoknya mencukupi untuk dibeli
            const product = await Product.findById(req.params.id) ;
            if (!product)
                throw new InvalidInputError('Produk yang ingin dibeli sudah tidak ada, mungkin sudah dihapus sebelumnya');
            if (product.stockProduct - getOngoingBuy(req.params.id) - data.jumlah < 0)
                throw new InvalidInputError('Barang tidak memiliki stok yang mencukupi untuk dibeli dengan jumlah yang diberikan');

            data.riwayatPembayaran = riwayatPembayaran;
            await data.save();

            decrementProductOngoingBuy(data.product._doc.data._id, data.jumlah);

            res.send({
                status: 'success',
                message: 'Barang berhasil dibayar'
            })
        })
        .catch(err => {
            Riwayat.findByIdAndRemove(riwayatPembayaran._id);
            handleError(errMessage, err, res)
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.checkout = async (req, res) => {
    const errMessage = 'Gagal melakukan Checkout'
    try {
        if (await validateInput(req, res, {
            'idProduct': {
                notEmpty: true
            },
            'jumlah': {
                notEmpty: true
            }
        }) == false) return;

        // Cek dulu apakah produknya ada dan stoknya mencukupi untuk dibeli
        const product = await Product.findById(req.body.idProduct) 
        if (!product)
            throw new InvalidInputError('Tidak ada produk dengan Id yang diberikan');
        if (product.stockProduct - getOngoingBuy(req.body.idProduct) - req.body.jumlah < 0)
            throw new InvalidInputError('Barang tidak memiliki stok yang mencukupi untuk dibeli dengan jumlah yang diberikan');

        // Jika semua kondisi di atas terpenuhi, lakukan proses checkout
        Riwayat.create({
            'type': 'jual',
            'product': {
                'status': 'exist',
                'data': req.body.idProduct
            },
            'tanggal': Date.now(),
            'hargaJual': await getHargaProduct(req.body.idProduct),
            'oleh': req.user.id,
            'jumlah': req.body.jumlah
        })
        .then(data => {
            incrementProductOngoingBuy(req.body.idProduct, req.body.jumlah);

            res.send({
                status: 'success',
                message: 'Checkout berhasil',
                id: data._id
            });
        })
        .catch(err => {
            handleError(errMessage, err, res)
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

const getHargaProduct = async (idProduk) => {
    const data = await Product.findById(idProduk)
        .exec();

    if (!data)
        throw new InvalidInputError("Tidak ada produk dengan Id yang diberikan");

    return data.hargaJual
}

/**
 * Should only be called when the server is starting
 */
exports.setupOngoingBuy = () => {
    const errMessage = 'Failed checking ongoing buy'
    try {
        global.ongoingBuy = {};

        Riwayat.find({ type: 'jual', riwayatPembayaran: null })
        .populate('product.data')
        .then(dataList => {
            dataList.forEach(data => {
                if (data.product._doc.status == 'deleted')
                    return;

                if (data.product._doc.data != null)
                    incrementProductOngoingBuy(data.product._doc.data.id, data.jumlah);
            });
        })
        .catch(err => {
            console.error(err);
        })
    } catch (err) {
        console.error(err);
    }
}


const incrementProductOngoingBuy = (id, jumlah) => {
    if (global.ongoingBuy[id] == undefined)
        global.ongoingBuy[id] = jumlah;
    else
        global.ongoingBuy[id] += jumlah;
}
const decrementProductOngoingBuy = (id, jumlah) => {
    if (global.ongoingBuy[id] == undefined)
        global.ongoingBuy[id] = 0;
    else
        global.ongoingBuy[id] -= jumlah;
}
const getOngoingBuy = (id) => {
    const ongoingBuy = global.ongoingBuy[id];
    if (ongoingBuy != undefined)
        return ongoingBuy;
    else
        return 0;
}