const Riwayat = global.database.riwayat;
const ExcelJS = require('exceljs');
const Utility = require("../helpers/utility.helpers");

/* const makeLaporan = async (tahun, bulan) => {
    const prepareTahun = (tahun) => {
        if (tahun in dataAkhir)
            return;

        dataAkhir[tahun] = {
            "pendapatan": 9,
            "pengeluaran": 0,
            "bulan": {}
        }
    }

    const prepareBulan = (tahun, bulan) => {
        if (bulan in dataAkhir[tahun].bulan)
            return;

        dataAkhir[tahun].bulan[bulan] = {
            "pendapatan": 0,
            "pengeluaran": 0,
            "tanggal": {}
        };
    }

    const prepareTanggal = (tahun, bulan, tanggal) => {
        if (tanggal in dataAkhir[tahun].bulan[bulan].tanggal)
            return;

        dataAkhir[tahun].bulan[bulan].tanggal[tanggal] = {
            "penjualan": {
                "pendapatan": 0,
                "barang": []
            },
            "pembelian": {
                "pengeluaran": 0,
                "barang": []
            }
        };
    }

    const processData = (riwayat, tanggal, bulan, tahun) => {
        const dataTahun = dataAkhir[tahun];
        const dataBulan = dataAkhir[tahun].bulan[bulan];
        const dataTanggal = dataAkhir[tahun].bulan[bulan].tanggal[tanggal];

        let pendapatan = 0;
        let pengeluaran = 0;
        
        if (riwayat.type == "beli") {
            pengeluaran += riwayat.hargaBeli * riwayat.jumlah;  
            dataTanggal.pembelian.barang.push(
                Object.assign({ jumlah: riwayat.jumlah, hargaBeli: riwayat.hargaBeli }, riwayat.product._doc)
            ); 
        }
        else if (riwayat.type == "jual") {
            pendapatan += riwayat.hargaJual * riwayat.jumlah;
            dataTanggal.penjualan.barang.push(
                { jumlah: riwayat.jumlah, hargaJual: riwayat.hargaJual, namaProduct: riwayat.product._doc.namaProduct }
            );
        }

        dataTahun.pendapatan += pendapatan;
        dataTahun.pengeluaran += pengeluaran;

        dataBulan.pendapatan += pendapatan;
        dataBulan.pengeluaran += pengeluaran;

        dataTanggal.penjualan.pendapatan += pendapatan;
        dataTanggal.pembelian.pengeluaran += pengeluaran;
    }

    let dataAkhir = {};

    // Wait for the query to complete and fetch all documents from the Transaksi collection
    const dataRiwayat = await Riwayat.find();

    for (const riwayat of dataRiwayat) {
        const tahun = riwayat.tanggal.getFullYear();
        const bulan = riwayat.tanggal.getMonth() + 1;
        const tanggal = riwayat.tanggal.getDate();

        console.log("Laporan > ", tahun, bulan, tanggal, riwayat.tanggal)

        prepareTahun(tahun);
        prepareBulan(tahun, bulan);
        prepareTanggal(tahun, bulan, tanggal);

        processData(riwayat, tanggal, bulan, tahun)
    }

    return dataAkhir;
} */

/* 
pendapatanPerBulan

{
    "2023": {
        "pendapatan": 42300000
        "pengeluaran": 12300000,
        "bulan": {
            "2": {
                "nama": "Februari",
                "pendapatan": 80000000,
                "pengeluaran": 12300000,
                "persentaseBarangTerjual": {
                    "25": {
                        "id": 1231232312,
                        "nama": "Topi",
                        "jumlah": 30,
                        "harga": {
                            "jual": 2500,
                            "beli": 2000
                        }
                    },
                    "75": {
                        "id": 231231232,
                        "nama": "Penghapus",
                        "jumlah": 45,
                        "harga": [
                            {
                                "tanggal": 1,
                                "harga": {
                                    "jual": 2000,
                                    "beli": 1500
                                }
                            },
                            {
                                "tanggal": 12,
                                "harga": {
                                    "jual": 2500,
                                    "beli": 1500
                                }
                            }
                        ]
                    }
                },
                "tanggal": {
                    "2": {
                        "perubahanHarga": [
                            {
                                "id": 1231312323,
                                "nama": "Spidol",
                                "jumlah": 10,
                                "harga": {
                                    "jual": 3000,
                                    "beli": 2500
                                }
                            }
                        ],
                        ""
                    },
                    "10": {
                        "penjualan": {
                            "pendapatan": {
                                "kotor": 50000000,
                                "bersih": 120000
                            },
                            "barang": [
                                {
                                    "id": 1231312323,
                                    "nama": "Spidol",
                                    "jumlah": 10,
                                    "harga": {
                                        "jual": 3000,
                                        "beli": 2500
                                    }
                                },
                                {
                                    "id": null,
                                    "nama": "Print",
                                    "jumlah": 5,
                                    "harga": {
                                        "jual": 500,
                                        "beli": 100
                                    }
                                }
                            ]
                        },
                        "pembelian": {
                            "pengeluaran": 82000,
                            "barang": [
                                {
                                    "nama": "Pensil",
                                    "jumlah": 200,
                                    "harga": {
                                        "jual": 2000,
                                        "beli": 1500
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
}

*/

/*
{
    "2023": {
        "bulan": {
            2: {

            }
        }
    }
}
*/

const makeWorksheet = async (adminName, laporan) => {
    try {
        const writeSheetPerBulan = (sheet, yOffset, namaBulan, dataBulan) => {
            let barangTerjualRows = [];
            let barangDibeliRows = [];
            console.log("> ", dataBulan)

            
            
            console.log("A > ", barangTerjualRows, barangDibeliRows)

            /* sheet.addTable({
                name: `Laporan Jual - ${namaBulan}`,
                ref: `A${yOffset}`,
                headerRow: true,
                style: {
                    theme: 'TableStyleMedium16',
                    showRowStripes: false
                },
                columns: [
                    { name: 'Barang terjual', filterButton: false },
                    { name: 'Jumlah', filterButton: true },
                    { name: 'Pendapatan', filterButton: true }
                ],
                rows: barangTerjualRows
            })
 */
            /* const jualHeaderTable = sheet.getRow(++yOffset);
            jualHeaderTable.values = ['Barang Terjual', 'Jumlah', 'Pendapatan']; */

            sheet.addRow(['Barang Terjual', 'Jumlah', 'Pendapatan']);

            for (const barangTerjual of dataBulan.barangTerjual) {
                sheet.addRow([barangTerjual.namaProduct, barangTerjual.jumlah, barangTerjual.hargaJual * barangTerjual.jumlah]);
                yOffset++;
            }

            sheet.addRow(['Barang Terjual', 'Jumlah', 'Pendapatan']);

            for (const barangDibeli of dataBulan.barangDibeli) {
                
            }



            return barangTerjualRows.length + 1;
        }

        const writeSheetPerTahun = (sheet, dataTahun) => {
            /* sheet.columns = [
                { header: 'Bulan', key: 'bulan' },
                { header: 'Barang terjual', key: 'barang_jual' },
                { header: 'Jumlah', key: 'barang_jual' },
                { header: 'Pendapatan', key: 'pendapatan' },
                { header: 'Total Pendapatan', key: 'pendapatan' },
                { header: 'Barang dibeli', key: 'barang_beli' },
                { header: 'Jumlah', key: 'barang_beli' },
                { header: 'Pengeluaran', key: 'pengeluaran' }
                { header: 'Total Pengeluaran', key: 'pengeluaran' }
            ] */

            // const ecooperLogo = workbook.addImage({
            //     filename: 'public/ecooper-logo.png',
            //     extension: 'png'
            // })
            // sheet.addImage(ecooperLogo, {
            //     tl: { col: 0, row: 0 },
            //     ext: { width: 394, height: 394 }
            // })

            let yOffset = 3;
            for (const bulan in dataTahun.bulan) {
                const dataBulan = dataTahun.bulan[bulan];

                yOffset += writeSheetPerBulan(sheet, yOffset, Utility.bulan[bulan], dataBulan) + 4;
            }
        }
    
        const workbook = new ExcelJS.Workbook();
        workbook.creator = adminName;
        //workbook.created = Date.now();
    
        workbook.views = [
            {
                x: 0, y: 0, width: 10000, height: 10000,
                firstSheet: 0, activeTab: 1
            }
        ];
    
        for (const tahun in laporan) {
            const sheet = workbook.addWorksheet(tahun);
            const dataTahun = laporan[tahun];
    
            writeSheetPerTahun(sheet, dataTahun)
    
            await workbook.xlsx.writeFile('C:/Users/Fadhil/Desktop/testSheet.xlsx');
        }
    } catch (error) {
        console.error("Failed making laporan workbook: ", error)
    }
}

const makeLaporan = async (tahun, bulan) => {
    const prepareTahun = (tahun) => {
        if (tahun in dataAkhir)
            return;

        dataAkhir[tahun] = {
            "pendapatan": 9,
            "pengeluaran": 0,
            "bulan": {}
        }
    }

    const prepareBulan = (tahun, bulan) => {
        if (bulan in dataAkhir[tahun].bulan)
            return;

        dataAkhir[tahun].bulan[bulan] = {
            "pendapatan": 0,
            "barangTerjual": [],
            "pengeluaran": 0,
            "barangDibeli": []
        };
    }

    const processData = (riwayat, bulan, tahun) => {
        const dataTahun = dataAkhir[tahun];
        const dataBulan = dataAkhir[tahun].bulan[bulan];

        let pendapatan = 0;
        let pengeluaran = 0;
        
        if (riwayat.type == "beli") {
            let dataBarang = { jumlah: riwayat.jumlah, hargaBeli: riwayat.hargaBeli };

            if (riwayat.product._doc.status == 'exist') {
                dataBarang = Object.assign(dataBarang, { namaProduct: riwayat.product._doc.data.namaProduct });
            } else {
                dataBarang = Object.assign(dataBarang, { namaProduct: riwayat.product._doc.namaProduct });
            }

            pengeluaran += riwayat.hargaBeli * riwayat.jumlah;
            dataBulan.barangDibeli.push(dataBarang);
        }
        else if (riwayat.type == "jual") {
            let dataBarang = { jumlah: riwayat.jumlah, hargaJual: riwayat.hargaJual };

            if (riwayat.product._doc.status == 'exist') {
                dataBarang = Object.assign(dataBarang, { namaProduct: riwayat.product._doc.data.namaProduct });
            } else {
                dataBarang = Object.assign(dataBarang, { namaProduct: riwayat.product._doc.namaProduct });
            }

            pendapatan += riwayat.hargaJual * riwayat.jumlah;
            dataBulan.barangTerjual.push(dataBarang);
        }

        dataTahun.pendapatan += pendapatan;
        dataTahun.pengeluaran += pengeluaran;

        dataBulan.pendapatan += pendapatan;
        dataBulan.pengeluaran += pengeluaran;
    }

    let dataAkhir = {};

    // Wait for the query to complete and fetch all documents from the Transaksi collection
    const dataRiwayat = await Riwayat.find().populate('product.data', 'namaProduct');

    for (const riwayat of dataRiwayat) {
        const tahun = riwayat.tanggal.getFullYear();
        const bulan = riwayat.tanggal.getMonth() + 1;

        prepareTahun(tahun);
        prepareBulan(tahun, bulan);

        processData(riwayat, bulan, tahun)
    }

    return dataAkhir;
}

exports.getLaporan = async (req, res) => {
    try {
        // Send the data as a JSON response
        const laporan = await makeLaporan();
        const worksheet = await makeWorksheet("Udin", laporan);
        res.json(laporan);
    } catch (err) {
        // Handle errors
        console.error("Failed making laporan: ", err)
        res.status(500).send({ status: 'failed', message: err.toString() });
    }
};