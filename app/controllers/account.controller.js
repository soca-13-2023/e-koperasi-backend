const fs = require('fs').promises;
const Account = global.database.account;
const jwt = require('jsonwebtoken');
const bcrpyt = require('bcrypt');
const { validateInput, handleError, ProhibitedError, InvalidInputError } = require('../helpers/utility.helpers')

exports.update = async (req,res) => {
    const errMessage = 'Akun gagal diupdate';
    try {
        if(req.user._id != req.body.id){
            return handleError(errMessage, new ProhibitedError('Anda tidak dapat merubah akun milik orang lain'), res);
        }
    
        Account.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
        .then(data => {
            if (!data) {
                return handleError(errMessage, '', res)
            }
            res.send({message: "Data berhasil di update"})
        })
        .catch(err => {
            handleError(errMessage, err, res)
        });
    } catch (err) {
        handleError(errMessage, err, res)
    }
}

exports.delete = (req, res) => {
    const errMessage = 'Akun gagal dihapus';
    try {
        const id = req.params.id;

        if(req.user._id != id && req.user.type != 'admin'){
            return handleError(errMessage, new ProhibitedError('Anda tidak dapat menghapus akun milik orang lain'), res);
        }
    
        Account.findByIdAndRemove(id)
        .then(data => {
            res.send({message: "Akun berhasil di Hapus"})
        })
        .catch(err => {
            handleError(errMessage, err, res);
        });
    } catch (err) {
        handleError(errMessage, err, res)
    }
};

exports.login = async (req, res) => {
    const errMessage = 'Gagal login';
    try {
        if (await validateInput(req, res, {
            'id': { notEmpty: true, isString: true },
            'password': { notEmpty: true, isString: true }
        }) == false) return;
    
        const { id, password } = req.body;
    
        const account = await Account.findOne({ _id: id }).exec();
        if (!account) {
            return handleError(errMessage, new ProhibitedError('Tidak ada akun dengan Id yang diberikan'), res);
        }
    
        bcrpyt.compare(password, account.password, (err, result) => {
            if (result != true) {
                return handleError(errMessage, new ProhibitedError('Password salah'), res);
            }
    
            const token = jwt.sign({
                id: account._id,
                type: account.type,
                name: account.name,
            }, process.env.TOKEN_SECRET, { expiresIn: '24h' });
    
            return res.cookie('token', token, { httpOnly: false, sameSite: true })
                .cookie('name', account.name)
                .status(200)
                .send({
                    status: 'success', 
                    message: 'Login berhasil',
                    accountName: account.name
                });
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.register = async (req, res) => {
    const errMessage = 'Gagal register';
    try {
        if (await validateInput(req, res, {
            id: { 
                notEmpty: true, 
                isString: true 
            },
            name: { 
                notEmpty: true, 
                isString: true 
            },
            type: { 
                custom: {
                options: (value) => {
                    if (value != "user" && value != "admin") throw new Error("Tipe harus antara user atau admin");
                    else return true;
                }
            }},
            password: { 
                notEmpty: true, 
                isString: true 
            }
        }) == false) return;

        console.log('Buat akun', req.body);
        
        if (req.user != undefined && req.user.id != undefined && req.body.type == 'admin' && req.user.type != 'admin') {
            return handleError(errMessage, new ProhibitedError('User tidak diperbolehkan untuk membuat akun Admin'), res);
        }
        bcrpyt.hash(req.body.password, 10, async (err, hash) => {
            if (err)
                return handleError(errMessage, err, res);

            const imagePath = 'public/foto/' + req.body.foto[0].newFilename;
            const image = await fs.readFile(imagePath);
            const base64Image = Buffer.from(image, 'binary').toString('base64');
            fs.rm(imagePath);

            Account.create(Object.assign(req.body, { password: hash, foto: base64Image }))
            .then(() => {
                return res.status(201).send({ status: 'success', message: 'Buat akun berhasil' });
            })
            .catch(err => {
                handleError(errMessage, err, res);
            });
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.logout = (req, res) => {
    const errMessage = 'Gagal register';
    try {
        res.clearCookie('token')
            .clearCookie('name')
            .status(201)
            .send({ status: 'success', message: 'Logout berhasil' });
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.checkIfLoggedIn = (req, res, next) => {
    const token = req.cookies.token;
    try {
        const user = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = user;
        next();
    } catch (error) {
        console.error("Failed checking user's identity, continued by giving a user credential with undefined value");
        req.user = undefined;
        next()
    }
}

exports.authenticateUser = (req, res, next) => {
    const errMessage = 'Gagal autentikasi user';
    const token = req.cookies.token;
    try {
        const user = jwt.verify(token, process.env.TOKEN_SECRET);
        if (user.type !== 'user') {
            return handleError(errMessage, new ProhibitedError('Hanya akun dengan tipe User yang dapat masuk kesini'), res);
        }
        req.user = user;
        next();
    } catch (err) {
        res.clearCookie('token');
        handleError(errMessage, err, res);
    }
}

exports.authenticateAdmin = (req, res, next) => {
    const errMessage = 'Gagal autentikasi admin';
    const token = req.cookies.token;
    try {
        const user = jwt.verify(token, process.env.TOKEN_SECRET);
        if (user.type !== 'admin')
            return handleError(errMessage, new ProhibitedError('Hanya akun dengan tipe Admin yang dapat masuk kesini'), res);

        req.user = user;
        next();
    } catch (err) {
        res.clearCookie('token');
        handleError(errMessage, err, res);
    }
}

exports.authenticateAll = (req, res, next) => {
    const errMessage = 'Gagal autentikasi semua';
    const token = req.cookies.token;
    try {
        const user = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = user;
        next();
    } catch (err) {
        console.error(err);
        res.clearCookie('token');
        handleError(errMessage, err, res);
    }
}

exports.getAccountInfo = async (req, res) => {
    const errMessage = 'Gagal mendapatkan info akun';
    try {
        let id = req.user.type == 'admin' ? 
            (req.params.id == undefined ? req.user.id : req.params.id) : req.user.id;
        /* if(req.user.id != req.params.id || req.user.type != 'admin'){
            handleError(errMessage, new ProhibitedError('Anda tidak dapat melihat info akun milik orang lain'), res);
        } */
    
        Account.findById(id)
        .then(data => {
            if (!data)
                return handleError(errMessage, new InvalidInputError('Tidak ada akun dengan Id yang diberikan'), res);

            res.json(data);
        })
        .catch(err => {
            handleError(errMessage, err, res);
        });
    } catch (err) {
        handleError(errMessage, err, res)
    }
}

// ==== Test ====
exports.testRegister = (account) => {
    bcrpyt.hash(account.password, 10, (err, hash) => {
        if (err) {
            return console.error('Failed when creating Account', err);
        }

        Account.create(Object.assign(account, { password: hash }))
        .then(() => {
            const token = jwt.sign({
                id: account._id,
                account: account.type,
                name: account.name
            }, process.env.TOKEN_SECRET, { expiresIn: '1h' });
    
            console.log('Berhasil buat akun')
        })
        .catch(err => {
            return console.error('Gagal buat akun', err);
        });
    });
}