const { validateInput, handleError } = require('../helpers/utility.helpers')
const Riwayat = global.database.riwayat;
const Product = global.database.product;

exports.create = (req, res) => {
    Riwayat.create(req.body)
        .then(() =>  res.send({
            status: 'success', 
            message: 'Data berhasil disimpan'
        }))
        .catch(err => {
            handleError('Failed creating Transaksi', err, res);
        });
};

exports.findAll = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find()
            .populate('product.data', ['_id', 'namaProduct'])
            .populate('oleh', ['_id', 'name'])
            .exec();

        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        // Handle errors
        handleError('Failed getting all Transaksi data', err, res);
    }
};

exports.show = (req, res) => {
    // Implement logic to find and send a specific Transaksi by ID
    const id = req.params.id;

    Riwayat
        .findById(id)
        .populate('product.data', ['_id', 'namaProduct'])
        .then(async data => {

            res.send(data)
        })
        .catch(err => res.status(500).send({ status: 'failed', message: err.toString()})) 
};

exports.update = (req, res) => {
    // Implement logic to update a Transaksi by ID
    const id = req.params.id;

    Riwayat.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
    .then(data => {
        if (!data) {
            console.error("Failed updating Transaksi");
            res.status(500).send({ status: 'failed', message: "Tidak Dapat Mengupdate Data" })
        }
        res.send({
            status: 'success', 
            message: 'Data berhasil disimpan'
        })
    })
    .catch(err => {
        console.error("Failed updating Transaksi: ", err);
        res.status(500).send({ status: 'failed', message: err.toString() })
    });
};

exports.delete = (req, res) => {
    // Implement logic to delete a Transaksi by ID
    const id = req.params.id;

    Riwayat.findByIdAndRemove(id)
    .then(data => {
        if (!data) {
            console.error("Failed updating Transaksi");
            res.status(500).send({ status: 'failed', message: "Tidak Dapat Menghapus Data"})
        }
        res.send({
            status: 'success', 
            message: 'Data berhasil dihapus'
        })
    })
    .catch(err => {
        console.error("Failed deleting Transaksi: ", err);
        res.status(500).send({ status: 'failed', message: err.toString() })
    });
};

exports.userBeli = (req, res) => {
    Riwayat.create(
            Object.assign(req.body, { type: 'jual', oleh: req.user.id })
        )
        .then(() =>  res.send({
            status: 'success', 
            message: 'Data berhasil disimpan'
        }))
        .catch(err => {
            console.error("Failed beli Transaksi: ", err);
            res.status(404).send({ status: 'failed', message: err.toString() });
        });
}

exports.getRiwayatByType = async (req, res) => {
    const type = req.params.type;

    const data = await Riwayat.find({ type: type })
        .exec();

    res.json(data);
}