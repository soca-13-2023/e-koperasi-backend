const { validateInput, handleError, InvalidInputError } = require('../helpers/utility.helpers')
const Product = global.database.product;
const Category = global.database.category;

exports.create = async (req, res) => {
    if (await validateInput(req, res, {
        'dijual': {
            notEmpty: true,
            isBoolean: true 
        },
        'namaProduct': {
            notEmpty: true,
            isString: true
        },
        'stockProduct': {
            notEmpty: true,
            isInt: true 
        },
        'hargaJual': {
            notEmpty: true,
            isInt: true
        },
        'category': {
            notEmpty: true,
            isString: true
        },
        'foto': {
            notEmpty: true
        }
    }) == false) return;

    const foto = 'foto/' + req.body.foto[0].newFilename;
    Product.create(Object.assign(req.body, { foto: foto }))
        .then(() => res.send({
            status: 'success',
            message: 'Data berhasil disimpan'
        }))
        .catch(err => {
            handleError('Failed creating product', err, res);
        });
};

exports.findAll = (req, res) => {
    if (req.query.detailed == 'true') {
        this.findAllDetailed(req, res);
    } else {
        this.findAllOptimized(req, res);
    }
};

exports.findAllOptimized = async (req, res) => {
    const errMessage = 'Failed getting Product Optimized';
    try {
        // Wait for the query to complete and fetch all documents from the Product collection
        const rawData = await Product.find()
            .populate('category', 'namaCategory')
            .exec();

        const optimizedData = [];

        rawData.forEach(product => {
            if (!product.dijual)
                return;
  
            optimizedData.push({
                id: product.id,
                namaProduct: product.namaProduct,
                stockProduct: product.stockProduct - getOngoingBuy(product.id),
                category: product.category,
                hargaJual: product.hargaJual,
                foto: product.foto
            });
        });

        // Send the data as a JSON response
        res.json(optimizedData);
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.findAllDetailed = async (req, res) => {
    const errMessage = 'Failed getting Product Detailed';
    try {
        // Wait for the query to complete and fetch all documents from the Product collection
        const data = await Product.find()
            .populate('category', 'namaCategory')
            .exec();

        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.findProductByCategoryAll = async (req, res) => {
    if (req.query.detailed == 'true') {
        this.findProductByCategoryDetailed(req, res);
    } else {
        this.findProductByCategoryOptimized(req, res);
    }
}

exports.findProductByCategoryOptimized = async (req, res) => {
    const errMessage = 'Failed finding Product by category Optimized';
    try {
        Product.find({ category: req.params.id })
        .populate('category', 'namaCategory')
        .then(async data => {
            if (!data) {
                return res.json([]);
            }

            const optimizedData = [];
            data.forEach(product => {
                if (!product.dijual)
                    return;
      
                optimizedData.push({
                    id: product.id,
                    namaProduct: product.namaProduct,
                    stockProduct: product.stockProduct - getOngoingBuy(product.id),
                    category: product.category,
                    hargaJual: product.hargaJual,
                    foto: product.foto
                });
            });

            res.send(optimizedData);
        })
        .catch(err => handleError(errMessage, err, res));
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.findProductByCategoryDetailed = async (req, res) => {
    const errMessage = 'Failed finding Product by category Detailed';
    try {
        Product.find({ category: req.params.id })
        .populate('category', 'namaCategory')
        .then(async data => res.send(data))
        .catch(err => handleError(errMessage, err, res));
    } catch (err) {
        handleError(errMessage, err, res);
    }
}

exports.showAll = (req, res) => {
    if (req.query.detailed == 'true') {
        this.showDetailed(req, res);
    } else {
        this.showOptimized(req, res);
    }
}

exports.showOptimized = (req, res) => {
    const errMessage = 'Failed showing Product';
    try {
        const id = req.params.id;

        Product.findById(id)
        .populate('category', 'namaCategory')
        .then(async data => {
            if (!data) {
                throw new InvalidInputError('Tidak ada Produk dengan Id yang diberikan');
            }

            optimizedData = {
                id: data.id,
                namaProduct: data.namaProduct,
                stockProduct: data.stockProduct - getOngoingBuy(data.id),
                category: data.category,
                hargaJual: data.hargaJual,
                foto: data.foto
            };

            res.send(optimizedData)
        })
        .catch(err => handleError(errMessage, err, res));
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.showDetailed = (req, res) => {
    const errMessage = 'Failed showing Product';
    try {
        const id = req.params.id;

        Product
        .findById(id)
        .populate('category', 'namaCategory')
        .then(async data => {
            if (!data) {
                throw new InvalidInputError('Tidak ada Produk dengan Id yang diberikan');
            }

            res.send(data)
        })
        .catch(err => handleError(errMessage, err, res));
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.update = async (req, res) => {
    const errMessage = 'Failed updating Product';
    try {
        if (await validateInput(req, res, {
            'id': {
                notEmpty: true,
                isString: true
            }
        }) == false) return;

        const id = req.params.id;

        let updateData;
        if (req.body.foto !== undefined) {
            const foto = 'foto/' + req.body.foto[0].newFilename;
            updateData = Object.assign(req.body, { foto: foto });
        } else {
            updateData = req.body;
        }
    
        Product.findByIdAndUpdate(id, updateData, { useFindAndModify: false })
        .then(data => {
            if (!data)
                throw new InvalidInputError('Tidak ada Produk dengan Id yang diberikan');

            res.send({
                status: 'success',
                message: 'Data berhasil disimpan'
            })
        })
        .catch(err => handleError(errMessage, err, res));
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.delete = (req, res) => {
    const errMessage = 'Failed deleting Product';
    try {
        const id = req.params.id;

        Product.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                throw new InvalidInputError('Tidak ada Produk dengan Id yang diberikan');
            }
            res.send({
                status: 'success',
                message: 'Data berhasil disimpan'
            })
        })
        .catch(err => handleError(errMessage));
    } catch (err) {
        handleError(errMessage, err, res);
    }
};


const getOngoingBuy = (id) => {
    const ongoingBuy = global.ongoingBuy[id];
    if (ongoingBuy != undefined)
        return ongoingBuy;
    else
        return 0;
};