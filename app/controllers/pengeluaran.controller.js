const Riwayat = global.database.riwayat;

exports.getTotal = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pengeluaran = 0;

        for (const transaksi of data) {
            pengeluaran += transaksi.harga * transaksi.jumlah;   
        }

        // Send the data as a JSON response
        res.json({
           pengeluaran
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ status: 'failed', message: err.toString() });
    }
};

exports.getPerYear = async (req, res) => {
    const year = req.params.year;

    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pengeluaran = 0;
        
        for (const transaksi of data) {
            if (transaksi.tanggal.getFullYear() != year)
                continue;

            pengeluaran += transaksi.harga * transaksi.jumlah; 
        }


        // Send the data as a JSON response
        res.json({
           pengeluaran
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ status: 'failed', message: err.toString() });
    }
};

exports.getPerMonth = async (req, res) => {
    const month = req.params.month;

    try {
        // Wait for the query to complete and fetch all documents from the Transaksi collection
        const data = await Riwayat.find();
        
        let pengeluaran = 0;
        
        for (const transaksi of data) {
            if (transaksi.tanggal.getMonth() + 1 != month)
                continue;

            pengeluaran += transaksi.harga * transaksi.jumlah; 
        }


        // Send the data as a JSON response
        res.json({
           pengeluaran
        });
    } catch (err) {
        // Handle errors
        res.status(500).send({ status: 'failed', message: err.toString() });
    }
};
