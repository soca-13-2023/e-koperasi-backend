const { validateInput, handleError, InvalidInputError } = require("../helpers/utility.helpers");
const db = require("../models");
const Product = db.product;
const Category = db.category;

exports.create = async (req, res) => {
    const errMessage = 'Failed creating category';
    try {
        if (await validateInput(req, res, {
            namaCategory: {
                notEmpty: true,
                isString: true
            }
        }) == false) return;
    
        const foto = 'foto/' + req.body.foto[0].newFilename;
        Category.create(Object.assign(req.body, { foto: foto }))
        .then(() => res.send({
            status: 'success', 
            message: 'Data berhasil disimpan'
        }))
        .catch(err => {
            handleError(errMessage, err, res);
        });
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.findAll = async (req, res) => {
    try {
        // Wait for the query to complete and fetch all documents from the Category collection
        const data = await Category.find();
        // Send the data as a JSON response
        res.json(data);
    } catch (err) {
        // Handle errors
        res.status(500).send({ status: 'failed', message: err.toString() });
    }
};

exports.show = (req, res) => {
    // Implement logic to find and send a specific Category by ID
    const id = req.params.id;

    Category.findById(id)
    .then(data => res.send(data))
    .catch(err => {
        handleError('Failed showing category', err, res);
    }); 
};

exports.update = (req, res) => {
    const errMessage = 'Failed updating category';
    try {
        const id = req.params.id;

        let updateData;
        if (req.file !== undefined) {
            const file = req.file?.path.replaceAll('\\', '/');
            updateData = Object.assign(req.body, { foto: file.split('/').slice(1).join('/') });
        } else {
            updateData = req.body;
        }
    
        Category.findByIdAndUpdate(id, updateData, {useFindAndModify: false})
        .then(data => {
            if (!data)
                throw new InvalidInputError('Tidak ada Kategori dengan Id yang diberikan');

            res.send({
                status: 'success', 
                message: 'Data berhasil disimpan'
            })
        })
        .catch(err => {
            handleError(errMessage, err, res);
        })
    } catch (err) {
        handleError(errMessage, err, res);
    }
};

exports.delete = (req, res) => {
    // Implement logic to delete a Category by ID
    const id = req.params.id;

    Category.findByIdAndRemove(id)
    .then(data => {
        if (!data) {
            res.status(500).send({ status: 'failed', message: "Tidak Dapat Menghapus Data"})
        }
        res.send({
            status: 'success', 
            message: 'Data berhasil dihapus'
        })
    })
    .catch(err => {
        handleError('Failed deleting catgory', err, res);
    });
};
