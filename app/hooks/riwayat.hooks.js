const { HookError, InvalidInputError } = require('../helpers/utility.helpers');

const changeProductStock = async (riwayatType, riwayatProduct) => {
    if (riwayatProduct.status == "deleted") {
        return true;
    } else if (riwayatProduct.status != "exist") {
        throw new InvalidInputError("Product status must be 'exist' or 'deleted'");
    }

    // Check if there is matching product in Product document
    const relatedProductList = await global.database.product
        .find({ _id: riwayatProduct.data });

    relatedProductList.forEach(relatedProduct => {
        // Calculate the product's stock
        let stockAkhir = 0;
        if (riwayatType == 'jual') {
            relatedProduct.stockProduct - relatedProduct.jumlah
        } else if (riwayatType == 'beli') {
            relatedProduct.stockProduct + relatedProduct.jumlah
        }

        relatedProduct.stockProduct = stockAkhir;
        relatedProduct.save();
    });

    if (relatedProductList.length > 0) {
        return true;
    } else {
        throw next(new InvalidInputError('Tidak ada produk dengan id yang diberikan'));
    }
}

// const pushBuyOngoing = (data) => {
//     global.buyOngoing[data.]
// }

const preSave = async (next, data) => {
    try {
        if ((data.type == 'jual' || data.type == 'beli') && data.product != undefined) {
            //await changeProductStock(data.type, data.product._doc);
        }

        // if (data.type == 'jual' && data.product != undefined && data.jumlah != undefined)
        //     pushBuyOngoing()

        next();
    } catch (err) {
        next(err);
    }
}
 
module.exports = (schema) => {
    schema.pre('save', function(next) {
        console.log('> Riwayat Hook pre save')
        preSave(next, this)
    });
    schema.pre('findOneAndUpdate', function(next) {
        console.log('> Riwayat Hook pre update \n')
        preSave(next, this.getUpdate())
    });
}