const { HookError } = require('../helpers/utility.helpers');

const removeAllReference = async (idProduct, next) => {
    const Riwayat = global.database.riwayat;

    await Riwayat
        .find({ 'product.data': idProduct })
        .populate('product.data', 'namaProduct')
        .cursor()
        .eachAsync((transaksi, i) => {
            if (transaksi.product.status != 'exist')
                return;

            transaksi.product = {
                status: 'deleted',
                namaProduct: transaksi.product._doc.data.namaProduct
            }
            
            transaksi.save();
        });

    next();
}

// Check if there is matching product in Product document
const validateData = async (next, data) => {
    if (data.category !== undefined && data.category !== "undefined") {
        const category = await global.database.category.findById(data.category).exec();
        if (!category) {
            next(new HookError('Tidak ada Category dengan id yang diberikan'));
            return;
        }
    }
    next();
}

module.exports = (schema) => {
    schema.pre('findOneAndRemove', function (next) {
        console.log('> Product Hook pre remove')
        removeAllReference(this.getFilter()._id, next);
    });
    schema.pre('remove', function (next) {
        console.log('> Product Hook pre remove');
        removeAllReference(this, next);
    });
    schema.pre('save', function(next) {
        console.log('> Product Hook pre save')
        validateData(next, this)
    });
    schema.pre('findOneAndUpdate', function(next) {
        console.log('> Product Hook pre update \n')
        validateData(next, this.getUpdate())
    });
}
