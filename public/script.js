window.addEventListener('load', _ => {
    const updateProductListButton = document.querySelector('#update-product-list-btn')
    const updateCategoryListButton = document.querySelector('#update-category-list-btn')

    updateProductListButton.addEventListener('click', updateProductList)
    updateCategoryListButton.addEventListener('click', updateCategoryList)

    updateProductList()
    updateCategoryList()

    testLogin()
})

const updateProductList = async () => {
    const productList = document.querySelector('#product-list')
    productList.innerHTML = ''
    
    const response = await fetch('/product')
    const data = await response.json()

    for (const product of data) {
        console.log(product)

        const productListItem = document.createElement('p')
        let content = ''
        
        for (const [key, value] of Object.entries(product)) {
            content += `${key}: ${JSON.stringify(value)} <br>`
        }

        productListItem.innerHTML = content

        const deleteButton = document.createElement('button')
        deleteButton.innerText = 'Hapus'
        deleteButton.addEventListener('click', () => deleteProduct(product.id))

        productListItem.append(deleteButton)
        productList.append(productListItem)
    }
}

const updateCategoryList = async () => {
    const categoryList = document.querySelector('#category-list')
    categoryList.innerHTML = ''
    
    const response = await fetch('/category')
    const data = await response.json()

    for (const category of data) {
        console.log(category)

        const productListItem = document.createElement('p')
        let content = ''
        
        for (const [key, value] of Object.entries(category)) {
            content += `${key}: ${JSON.stringify(value)} <br>`
        }

        productListItem.innerHTML = content

        const deleteButton = document.createElement('button')
        deleteButton.innerText = 'Hapus'
        deleteButton.addEventListener('click', () => deleteCategory(category.id))

        productListItem.append(deleteButton)
        categoryList.append(productListItem)
    }
}

const deleteProduct = async (id) => {
    const response = await fetch(`/product/${id}`, { method: 'DELETE' })
    if (!response.ok) {
        alert(`Failed deleting product with id: ${id}`)
        return
    }

    const result = await response.json()

    console.log(result)
    alert(JSON.stringify(result))

    updateProductList()
}

const deleteCategory = async (id) => {
    const response = await fetch(`/category/${id}`, { method: 'DELETE' })
    if (!response.ok) {
        alert(`Failed deleting category with id: ${id}`)
    }

    const result = await response.json()

    console.log(result)
    alert(JSON.stringify(result))
    
    updateCategoryList()
}


const testLogin = async () => {
    const response = await fetch(`/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: prompt('Masukkan ID'),
            password: prompt('Masukkan password')
        })
    })
    if (!response.ok) {
        alert(`Failed login ${response.status}/${response.statusText} `)
    }

    const result = await response.json()

    console.log(result)
    alert(JSON.stringify(result))
}