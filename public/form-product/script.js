window.addEventListener('load', _ => {
    updateCategorySelect()
})

const updateCategorySelect = async () => {
    const categorySelect = document.querySelector('#category-select')
    categorySelect.innerHTML = ''
    
    const response = await fetch('http://localhost:8085/category')
    const data = await response.json()

    for (const category of data) {
        console.log(category)

        const categoryOption = document.createElement('option')
        
        categoryOption.innerText = `${category.namaCategory} - ${category.id}`
        categoryOption.setAttribute('value', category.id)

        categorySelect.append(categoryOption)
    }
}