const readline = require("readline")
const express = require("express")
const cors = require("cors")
const { formidable } = require("formidable")
const db = require("./app/models")
const cookieParser = require("cookie-parser")
const accountController = require("./app/controllers/account.controller")
const { setupOngoingBuy } = require('./app/controllers/pembayaran.controller')
require('dotenv').config()

const corsOptions = {
    origin: ["http://localhost:8080", "http://localhost:8081", "http://localhost:8082"],
    credentials: true
}

const cli = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const parseForm = (req, res, next) => {
    const form = formidable({ uploadDir: 'public/foto' });

    form.parse(req, (err, fields, files) => {
        if (err) {
            return next(err);
        }

        req.body = files;
        for (field in fields) {
            if (fields[field] instanceof Array)
                req.body[field] = fields[field][0];
            else
                req.body[field] = fields[field];
        }
        next();
    });
}

//register cors middleware
const app = express()
app.use(cors(corsOptions))
app.use(parseForm);
// app.use(express.json())
// app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser());

// Test form
app.use(express.static('public'))

// Konek database
const mongooseConfig = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

db.mongoose.connect(db.url, mongooseConfig)
    .then(() => console.log("Database connected"))
    .catch(err => {
        console.error("Failed to connect to database: ", err.message)
        process.exit()
    });

// Memanggil route mahasiswa
require("./app/routes/routes")(app);
setupOngoingBuy();

const PORT = process.env.port || 8085
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));


const askCommand = () => {
    cli.question('> ', (command) => {
        const args = command.split(' ')

        if (args[0] == 'admin') {
            if (args.length < 3) {
                console.log('Masukkan id, nama, dan password secara berurutan!')
                return askCommand()
            }
            accountController.testRegister({
                type: 'admin',
                _id: args[1],
                name: args[2],
                password: args[3],
                foto: args[4]
            })
        }
        else {
            console.error('salah')
            askCommand()
            return 0
        }
    })
}; askCommand()
